# [1.2.0](https://gitlab.com/dblaisonneau/social-poster/compare/1.1.0...1.2.0) (2023-01-03)


### Features

* generic class ([c014b0d](https://gitlab.com/dblaisonneau/social-poster/commit/c014b0d03313b598bea8118c0e55d10fe12fb210))

# [1.1.0](https://gitlab.com/dblaisonneau/social-poster/compare/1.0.0...1.1.0) (2022-12-29)


### Features

* add mastodon lib ([9808ba7](https://gitlab.com/dblaisonneau/social-poster/commit/9808ba717cb27213d556dff654b474c0d835b07f))

# 1.0.0 (2022-12-29)


### Features

* add twitter lib ([dc011fb](https://gitlab.com/dblaisonneau/social-poster/commit/dc011fb22c56240ab16e538b09b88f2e88ca95f3))
