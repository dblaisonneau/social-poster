"""
Mastodon library.

It contains the MastodonPoster class to post statuses on Mastodon.
"""

from socialposter.generic_poster import GenericPoster
from socialposter.lib import get_env_var_or_die

class MastodonPoster(GenericPoster):
    '''Class to connect an interact with a mastodon server.'''

    def __init__(self):
        '''Init mastodonPoster class.'''
        self.host = get_env_var_or_die("mastodon_host")
        self.token = get_env_var_or_die("mastodon_token")
        GenericPoster.__init__(self)

    def post_status(self, text, media_ids=None):
        '''Post a status message with an optional image.'''
        data = {
            "status": text,
            "media_ids[]": media_ids
        }
        url = f"{self.host}/api/v1/statuses"
        response = self.request(url, data)
        return response['url']

    def post_media(self, image_url):
        '''Push an image to the server.'''
        self.info(f"post media with  {image_url}")
        media = self.fetch_remote_media(image_url)
        data = {
                'description': 'Attached file:' + image_url
            }
        with open(media, 'rb') as file:
            files = {
                'file': (
                    image_url.split('/')[-1],
                    file,
                    'application/octet-stream')
            }
            url = f"{self.host}/api/v1/media"
            response = self.request(url, data, files=files)
        return response['id']
