"""
Poster library.

Generic poster object.
"""

import logging
from urllib.request import urlopen
from shutil import copyfileobj
import os.path
import requests

DEFAULT_FILENAME = 'media'

# pylint: disable=too-many-instance-attributes
class GenericPoster:
    '''Class to connect an interact with a social server.'''

    def __init__(self):
        '''Init GenericPoster class.'''
        if not hasattr(self, 'token'):
            self.token = None
        self.info("init")
        self.init_auth()

    def get_class_name(self):
        '''Return the class name for logging prefix'''
        return type(self).__name__

    def init_auth(self):
        '''Authenticate to the server.'''
        self.info("no pre-auth, using bearer token")

    def send_message(self, text, image_url=None):
        '''Send a message with an optional image.'''
        self.info("prepare message")
        media_ids = None
        if image_url:
            media_ids = [self.post_media(image_url)]
        self.info("send message")
        msg_url = self.post_status(text, media_ids)
        self.info(f"message is available at {msg_url}")

    def post_status(self, text, media_ids):
        '''Post a status message with an optional image - Generic function.'''
        self.info(f"post status with text {text} and media {media_ids}"
            " - Generic function, shall not be runned")
        return "Generic function, no URL"

    def post_media(self, image_url):
        '''Push an image to the server - Generic function.'''
        self.info(f"post media with url {image_url}"
            " - Generic function, shall not be runned")

    def fetch_remote_media(self, image_url, filename=None):
        '''Fetch the remote media from url.'''
        if filename is None:
            filename = image_url.split('/')[-1]
        if os.path.exists(filename):
            self.info(f"media with url {image_url} already exists")
        else:
            self.info(f"download media with url {image_url}")
            with (urlopen(image_url) as in_stream,
                open(filename, 'wb') as out_file):
                copyfileobj(in_stream, out_file)
        self.info(f"media file is {filename}")
        return filename

    def info(self, log):
        '''Log interface with INFO level.'''
        logging.info("%s: %s", self.get_class_name(), log)

    def error(self, log):
        '''Log interface with ERROR level.'''
        logging.error("%s: %s", self.get_class_name(), log)

    def debug(self, log):
        '''Log interface with DEBUG level.'''
        logging.debug("%s: %s", self.get_class_name(), log)

    def request(self, url, data, ok_status="200", files=None):
        '''POST to an url using request.'''
        self.info(f"request to {url}")
        req = requests.post(
                url,
                data=data,
                files=files,
                headers={'Authorization': f"Bearer {self.token}"},
                timeout=60)
        if f"{req.status_code}" is not ok_status:
            self.error(f"request response is {req.status_code} "
                f"shall be {ok_status})")
        self.debug(req.json())
        return req.json()
