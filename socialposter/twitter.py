"""
Twitter library.

It contains the TwitterPoster class to post statuses on Twitter.
"""

import tweepy
from socialposter.generic_poster import GenericPoster
from socialposter.lib import get_env_var_or_die


# pylint: disable=too-many-instance-attributes
class TwitterPoster(GenericPoster)  :
    '''Class to connect an interact with a twitter server.'''

    def __init__(self):
        '''Init TwitterPoster class.'''
        self.consumer_key = get_env_var_or_die("twitter_consumer_key")
        self.consumer_secret = get_env_var_or_die("twitter_consumer_secret")
        self.access_token = get_env_var_or_die("twitter_access_token")
        self.access_token_secret = get_env_var_or_die("twitter_access_token_secret")
        GenericPoster.__init__(self)

    def init_auth(self):
        '''Authenticate to the server.'''
        self.info("auth")
        # Api v2 - for easy tweet
        self.client = tweepy.Client(
            consumer_key=self.consumer_key,
            consumer_secret=self.consumer_secret,
            access_token=self.access_token,
            access_token_secret=self.access_token_secret
        )
        # Api v1 - for media
        self.auth = tweepy.OAuth1UserHandler(
            self.consumer_key,
            self.consumer_secret,
            self.access_token,
            self.access_token_secret)
        self.api = tweepy.API(self.auth)

    def post_status(self, text, media_ids=None):
        '''Send a message with an optional image.'''
        response = self.client.create_tweet(
            text=text,
            media_ids=media_ids)
        return f"https://twitter.com/user/status/{response.data['id']}"

    def post_media(self, image_url):
        '''Push an image to the server.'''
        self.info(f"post media with url {image_url}")
        media = self.fetch_remote_media(image_url)
        return self.api.media_upload(media).media_id
