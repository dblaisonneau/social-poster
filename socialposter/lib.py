'''
Common functions.
'''

import os
import logging


def get_env_var(var_name):
    '''return an environment variable.'''
    return os.environ.get(var_name)

def get_env_var_or_die(var_name):
    '''return an environment variable or die if None.'''
    var = get_env_var(var_name)
    logging.info("getEnvVarOrDie: %s", var_name)
    if var is None:
        raise Exception(f"Environment variable '{var_name}' must be set")
    return var
