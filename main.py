#!/usr/bin/python3

"""
Main routine of socialPoster.
"""

import logging
from socialposter.mastodon import MastodonPoster
from socialposter.twitter import TwitterPoster

def main():
    '''Post message on all social medias.'''

    message = "Test message"
    image = "https://media1.giphy.com/media/gw3IWyGkC0rsazTi/giphy.gif"

    logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)
    logging.info("Init social poster")

    mastodon = MastodonPoster()
    mastodon.send_message(message, image)
    twitter = TwitterPoster()
    twitter.send_message(message, image)

if __name__ == "__main__":
    main()
